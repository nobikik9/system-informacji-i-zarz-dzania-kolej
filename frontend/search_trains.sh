#!/bin/bash

user=`whoami`;

#pierwszy parametr - nazwa stacji 1
#drugi parametr - nazwa stacji 2

if [ $# != 2 ]; then
    echo "Podaj nazwy stacji początkowej i końcowej!"
    exit 1
fi

psql -X -P pager=off -U $user -f search_trains.sql -v QSTACJA1=\'$1\' -v QSTACJA2=\'$2\' koleje;
