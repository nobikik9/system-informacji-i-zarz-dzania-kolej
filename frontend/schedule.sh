#!/bin/bash

user=`whoami`;

#pierwszy parametr - nazwa stacji

if [ $# != 1 ]; then
    echo "Podaj nazwę stacji!"
    exit 1
fi

echo "$1 - ODJAZDY"
echo "-----------------------------------"
psql -X -P pager=off -U $user -f schedule_departures.sql -v QSTACJA=\'"$1"\' koleje;

echo "$1 - PRZYJAZDY"
echo "-----------------------------------"
psql -X -P pager=off -U $user -f schedule_arrivals.sql -v QSTACJA=\'"$1"\' koleje;
