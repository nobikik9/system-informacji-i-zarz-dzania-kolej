SELECT to_char(a.czas_przyjazdu, 'HH:MI') as godzina, trasa as id
FROM (trasy_odcinki JOIN stacje ON stacja_poczatkowa=id_stacji) as a JOIN (trasy_odcinki JOIN stacje ON stacja_koncowa=id_stacji) as b USING(trasa) 
WHERE a.nazwa=:QSTACJA1 AND b.nazwa=:QSTACJA2 AND a.czas_odjazdu < b.czas_odjazdu;
