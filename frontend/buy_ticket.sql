begin;
SELECT kup_bilet(nowy_pasazer(:QIMIE::text, :QNAZWISKO::text, :QEMAIL::text), :POCIAG, :QSTACJA1::text, :QSTACJA2::text, :ULGA);
SELECT nazwa || ' ' || to_char(czas_odjazdu, 'HH:MI') as "OD:"
  FROM bilety JOIN trasy_odcinki ON trasy_odcinki_od = id_trasy_odcinki JOIN stacje ON stacja_poczatkowa = id_stacji
  WHERE id_biletu=lastval();

SELECT nazwa || ' ' || to_char(czas_przyjazdu, 'HH:MI') as "DO:"
  FROM bilety JOIN trasy_odcinki ON trasy_odcinki_do = id_trasy_odcinki JOIN stacje ON stacja_koncowa = id_stacji
  WHERE id_biletu=lastval();

SELECT 'wagon: ' || id_sklady_wagony::text || ' miejsce: ' || miejsce_w_wagonie || ' od okna? ' || od_okna(miejsce_w_wagonie)::text as "MIEJSCE:"
 FROM bilety
  WHERE id_biletu=lastval();

SELECT nazwa || ' ' || procent::text || '%' as "ULGA"
FROM bilety JOIN ulgi USING(id_ulgi)
WHERE id_biletu=lastval();

SELECT cena_biletu(lastval()::int)::text || ' zł' AS "CENA";


commit;
