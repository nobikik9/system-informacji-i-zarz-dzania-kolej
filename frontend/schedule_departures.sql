SELECT to_char(czas_odjazdu, 'HH:MI') as odjazd,
(
	SELECT string_agg(to_char(czas_odjazdu, 'HH:MI') || ' ' || nazwa, ' ' ORDER BY czas_odjazdu) 
	FROM trasy_odcinki JOIN stacje ON stacja_poczatkowa=id_stacji 
	WHERE trasa=f.trasa AND czas_odjazdu > f.czas_odjazdu
) as "odjazdy ze stacji pośrednich"
FROM (trasy_odcinki JOIN stacje ON stacja_poczatkowa=id_stacji) f
WHERE f.nazwa=:QSTACJA
ORDER BY 1;
