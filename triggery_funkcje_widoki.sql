BEGIN;

-- Funkcja, która wyznacza cenę biletu
-- TODO: dopisać usuwanie w clear.sql

CREATE OR REPLACE FUNCTION cena_biletu(bilet int) RETURNS numeric AS
$$
DECLARE
  cena numeric DEFAULT 0;
  trasa_biletu int;
  ulga int;
  przewoznik int;
  czas_od TIMESTAMP;
  czas_do TIMESTAMP;
BEGIN
  IF NOT EXISTS (SELECT * FROM bilety WHERE id_biletu=bilet) THEN
    RAISE EXCEPTION 'Nie istnieje takiego biletu!';
  END IF;
  ulga = (SELECT COALESCE(procent, 0) FROM bilety NATURAL JOIN ulgi WHERE id_biletu=bilet);
  SELECT INTO czas_od, trasa_biletu
      czas_odjazdu, trasa
          FROM trasy_odcinki
          WHERE id_trasy_odcinki=(SELECT trasy_odcinki_od FROM bilety WHERE id_biletu=bilet);
  czas_do = (SELECT czas_przyjazdu
          FROM trasy_odcinki
          WHERE id_trasy_odcinki=(SELECT trasy_odcinki_do FROM bilety WHERE id_biletu=bilet));
  cena = (SELECT SUM(dlugosc)
          FROM trasy_odcinki
            NATURAL JOIN odcinki
          WHERE czas_odjazdu >= czas_od AND czas_przyjazdu <= czas_do AND trasa_biletu=trasa);
  przewoznik = (SELECT id_przewoznika FROM trasy WHERE id_trasy=trasa_biletu);
  RETURN cena*(100-ulga)/100*(SELECT stawka FROM przewoznicy WHERE id_przewoznika=przewoznik);
END;
$$
LANGUAGE plpgsql;

-- Dodanie triggeru (podczas zakupu biletu)
/*
CREATE OR REPLACE FUNCTION zajmij_miejsce() RETURNS TRIGGER AS
$$
DECLARE
  lim int;
  typ_wagonu int;
  cnt int;
BEGIN
  typ_wagonu = (SELECT wagon FROM sklady_wagony WHERE NEW.id_sklady_wagony=sklady_wagony.id_sklady_wagony);
  lim = (SELECT ilosc_miejsc FROM typy_wagonow WHERE id_typ_wagonu=typ_wagonu);
  IF EXISTS(SELECT * FROM bilety WHERE NEW.id_sklady_wagony=id_sklady_wagony AND NEW.miejsce_w_wagonie=miejsce_w_wagonie) THEN
    RETURN NULL;
  END IF;
  cnt = (SELECT COUNT(*) FROM bilety WHERE NEW.id_sklady_wagony=id_sklady_wagony);
  IF cnt >= lim THEN
    RETURN NULL;
  END IF;
  RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER unikalnosc_miejsca BEFORE INSERT OR UPDATE ON bilety
  EXECUTE PROCEDURE zajmij_miejsce();
*/
-- Wyznaczamy, czy miejsce jest od okna

CREATE OR REPLACE FUNCTION od_okna(miejsce int) RETURNS BOOLEAN AS
$$
BEGIN
  IF miejsce < 0 OR miejsce > 300 THEN
    RAISE EXCEPTION 'Nieprawidłowy numer miejsca';
  END IF;
  IF miejsce % 10 IN (1,2,5,6) THEN
    RETURN TRUE;
  END IF;
  RETURN FALSE;
END;
$$
LANGUAGE plpgsql;
/*
CREATE OR REPLACE FUNCTION poprzedni_odcinek(czas TIMESTAMP, id_trasy int) RETURNS int AS
$$
BEGIN
  IF EXISTS (SELECT * FROM trasy_odcinki WHERE trasa=id_trasy) THEN
    RETURN (
      SELECT id_trasy_odcinki
      FROM trasy_odcinki
      WHERE czas_przyjazdu=(
        SELECT MIN(czas_przyjazdu)
        FROM trasy_odcinki
        WHERE id_trasy=trasa
      ) AND id_trasy=trasa
    );
  ELSE
    RETURN -1;
  END IF;
END;
$$
LANGUAGE plpgsql;

-- Rzucamy pociąg na wolny tor

CREATE OR REPLACE FUNCTION wez_wolny_tor(stacja int, id_trasy int) RETURNS int AS
$$
DECLARE
  czas_od TIMESTAMP;
  czas_do TIMESTAMP;
BEGIN
  IF (!EXISTS(SELECT ))
    czas_od = NULL;
  ELSE
    czas_od =

END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION wolny_tor() RETURNS TRIGGER AS
$$
DECLARE
  poprzednik int;
BEGIN
  poprzednik = poprzedni_odcinek(NEW.czas_odjazdu, NEW.trasa);
  IF poprzednik IS NULL THEN
    NEW.tor_poczatkowy=wez_wolny_tor(NEW.stacja_poczatkowa, NEW.trasa);
  ELSE
    NEW.tor_poczatkowy=(SELECT tor_koncowy FROM trasy_odcinki WHERE id_trasy_odcinki=poprzednik);
  END IF;
  NEW.tor_koncowy=wez_wolny_tor(NEW.stacja_poczatkowa, NEW.trasa);
COMMIT;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER daj_wolny_tor BEFORE INSERT ON trasy_odcinki
  EXECUTE PROCEDURE wolny_tor();
*/

/* tworzy nowego pasażera
jeżeli wszystkie pola są null to zwraca null
w przeciwnym wypadku zwraca id nowo utworzonego pasażera
*/

CREATE OR REPLACE FUNCTION nowy_pasazer(imie text, nazwisko text) RETURNS int AS
$$
DECLARE 
  tmp int;
BEGIN
  IF imie IS NULL AND nazwisko IS NULL THEN 
    RETURN NULL; 
  END IF;
  INSERT INTO pasazerowie (imie, nazwisko) VALUES(imie, nazwisko) RETURNING id_pasazera INTO tmp;
  RETURN tmp;
END;
$$
LANGUAGE plpgsql;


/* kupuje bilet i zwraca jego id*/

CREATE OR REPLACE FUNCTION kup_bilet(id_pasazera int, trasa_id int, stacja1 text, stacja2 text) RETURNS void AS
$$
BEGIN
  IF (SELECT COUNT(*) FROM bilety JOIN trasy_odcinki ON trasy_odcinki_od = id_trasy_odcinki WHERE trasa = trasa_id) = 
  (SELECT SUM(ilosc_miejsc) FROM trasy JOIN sklady_wagony USING(sklad) JOIN typy_wagonow ON wagon = id_typ_wagonu WHERE id_trasy=trasa_id) THEN
    INSERT INTO bilety(id_pasazera, trasy_odcinki_od, trasy_odcinki_do) VALUES(
	id_pasazera,
	(SELECT id_trasy_odcinki FROM trasy_odcinki JOIN stacje ON stacja_poczatkowa = id_stacji WHERE trasa = trasa_id AND nazwa=stacja1),
	(SELECT id_trasy_odcinki FROM trasy_odcinki JOIN stacje ON stacja_koncowa = id_stacji WHERE trasa = trasa_id AND nazwa=stacja2)
    );
  ELSE
    INSERT INTO bilety(id_pasazera, trasy_odcinki_od, trasy_odcinki_do, id_sklady_wagony, miejsce_w_wagonie) VALUES(
	id_pasazera,
	--usunąć to min po tym jak wojtek poprawi te trasy!
	(SELECT MIN(id_trasy_odcinki) FROM trasy_odcinki JOIN stacje ON stacja_poczatkowa = id_stacji WHERE trasa = trasa_id AND nazwa=stacja1),
	--tu też
	(SELECT MIN(id_trasy_odcinki) FROM trasy_odcinki JOIN stacje ON stacja_koncowa = id_stacji WHERE trasa = trasa_id AND nazwa=stacja2),
	(SELECT MIN(id_sklady_wagony) FROM (trasy JOIN sklady_wagony USING(sklad) JOIN typy_wagonow ON wagon = id_typ_wagonu) f WHERE id_trasy=trasa_id AND (SELECT COUNT(*) FROM bilety WHERE id_sklady_wagony = f.id_sklady_wagony) < ilosc_miejsc),
	0
    );
  END IF;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION bilet_ulga_chk() RETURNS TRIGGER AS $$
DECLARE
    ulga record;
BEGIN
    SELECT * INTO ulga FROM ulgi WHERE id_ulgi=NEW.id_ulgi;
    IF NOT ulga.obowiazuje THEN
        RAISE EXCEPTION 'Nie można wystawić biletu na nieobowiązującą ulgę %: % [% %%]', ulga.id_ulgi, ulga.nazwa, ulga.procent;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER bilet_ulga BEFORE INSERT OR UPDATE ON bilety
    FOR EACH ROW EXECUTE PROCEDURE bilet_ulga_chk();

COMMIT;
