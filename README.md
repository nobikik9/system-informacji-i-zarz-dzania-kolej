# System informacji i zarządzania koleją #
[*(Repozytorium)*](https://bitbucket.org/nobikik9/system-informacji-i-zarz-dzania-kolej)

## Wykonawcy ##
1. Angelika Serwa
2. Wojciech Śmigielski
3. Rafał Burczyński
4. Vladyslav Hlembotskyi

## Instrukcja ##
`./setup.sh` tworzy bazę i wypełnia ją danymi, domyślnie baza to `koleje` a użytkownik to aktualnie zalogowany użytkownik systemu

`cd frontend`

`./schedule.sh <stacja>` pokazuje rozkład jazdy dla stacji o nazwie `<stacja>`

`./buy_ticket.sh` kupowanie biletu

`./search_train.sh <stacja1> <stacja2>` szuka pociągów pomiędzy stacjami o nazwach `<stacja1>` `<stacja2>`