begin;

insert into przewoznicy values(1,'Przewozy Regionalne',0.2);
insert into przewoznicy values(2,'Koleje Mazowieckie',0.21);
insert into przewoznicy values(3,'PKP SKM w Trójmieście',0.22);
insert into przewoznicy values(4,'PKP Intercity',0.23);
insert into przewoznicy values(5,'SKM w Warszawie',0.19);
insert into przewoznicy values(6,'Koleje Śląskie',0.18);
insert into przewoznicy values(7,'Warszawska Kolej Dojazdowa',0.17);
insert into przewoznicy values(8,'Koleje Wielkopolskie',0.19);
insert into przewoznicy values(9,'Koleje Dolnośląskie',0.20);
commit;
