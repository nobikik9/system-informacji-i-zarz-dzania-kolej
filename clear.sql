/*
  Przykładowy skrypt, służący do czyszczenia bazy danych
*/

BEGIN;

-- Usuwanie pociągów
DROP TABLE IF EXISTS typy_lokomotyw CASCADE;
DROP TABLE IF EXISTS typy_wagonow CASCADE;
DROP TABLE IF EXISTS sklady_wagony CASCADE;
DROP TABLE IF EXISTS sklady CASCADE;

--Usuwanie tras, stacji, odcinkow
DROP TABLE IF EXISTS stacje CASCADE;
DROP TABLE IF EXISTS odcinki CASCADE;
DROP TABLE IF EXISTS trasy CASCADE;
DROP TABLE IF EXISTS trasy_odcinki CASCADE;

-- Usuwanie przwoznikow
DROP TABLE IF EXISTS przewoznicy CASCADE;

--Usuwanie pasażerów i t.d.
DROP TABLE IF EXISTS pasazerowie CASCADE;
DROP TABLE IF EXISTS ulgi CASCADE;
DROP TABLE IF EXISTS bilety CASCADE;

--Usuwanie funckji
DROP FUNCTION IF EXISTS cena_biletu(int);
DROP FUNCTION IF EXISTS zajmij_miejsce(int, int);
DROP FUNCTION IF EXISTS od_okna(int);
DROP FUNCTION IF EXISTS wolny_tor();

COMMIT;