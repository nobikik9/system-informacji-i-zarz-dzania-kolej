#!/bin/bash

# Skrypt służy do wypełniania bazy danych...
# Pierwszy parametr - nazwa bazy danych
# Drugi parametr - użytkownik

database="$1";
if [[ "$database" == "" ]]; then
    database=`whoami`;
fi;

user="$2";
if [[ "$user" == "" ]]; then
    user=`whoami`;
fi;

psql -d $database $user < clear.sql;
psql -d $database $user < create.sql;
psql -d $database $user < triggery_funkcje_widoki.sql

cd rzeczy_do_generowania;

# Wypełnianie przewoźników
psql -d $database $user < przewoznicy.sql;

# Wypełnianie pociągów
psql -d $database $user < typy_lokomotyw.sql; 
psql -d $database $user < typy_wagonow.sql;
psql -d $database $user < sklady.sql;

# Wypełnianie pasażerów i biletów
psql -d $database $user < pasazerowie.sql;
psql -d $database $user < ulgi.sql;

# Wypełniania stacji, odcinków i tras
psql -d $database $user < stacje.sql;
psql -d $database $user < odcinki.sql;
psql -d $database $user < trasy.sql;
psql -d $database $user < trasy_odcinki.sql;


# Wypełniania składy_wagony
psql -d $database $user < sklady_wagony.sql;


