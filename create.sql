/*
  Przykładowy skryp, tworzący bazę danych
*/

BEGIN;

-- Blok przewoźników
-- Stawka za kilometr długości

CREATE TABLE przewoznicy (
  id_przewoznika SERIAL PRIMARY KEY,
  nazwa VARCHAR(50) NOT NULL,
  stawka NUMERIC(5,2) NOT NULL
);

/*
  Block pociągów.
  Każdy skład - to jest lokomotywa + wagony
  Lokomotyw jest kilka typów
*/

-- Lokomotywy

-- Dodano przękość

CREATE TABLE typy_lokomotyw (
  id_typ_lokomotywy SERIAL PRIMARY KEY,
  predkosc numeric(5,2) CHECK(predkosc BETWEEN 10 AND 1000),
  nazwa VARCHAR(50) NOT NULL
);

-- Wagony

-- Dla wagonu restauracyjnego - ilość miejsc = 0 i on jest przedziałowy.

CREATE TABLE typy_wagonow (
  id_typ_wagonu SERIAL PRIMARY KEY,
  nazwa VARCHAR(50) NOT NULL,
  przedzialowosc VARCHAR(50) CHECK(przedzialowosc IN ('Przedziałowy', 'Bezprzedziałowy')),
  ilosc_miejsc INTEGER CHECK (ilosc_miejsc BETWEEN 1 AND 300)
);

-- Każdy skład składa się z lokomotywy i wagonów

CREATE TABLE sklady (
  id_skladu SERIAL PRIMARY KEY,
  id_lokomotywy int NOT NULL REFERENCES typy_lokomotyw(id_typ_lokomotywy)
);

CREATE TABLE sklady_wagony (
  id_sklady_wagony SERIAL PRIMARY KEY,
  wagon int NOT NULL REFERENCES typy_wagonow(id_typ_wagonu),
  sklad int NOT NULL REFERENCES sklady(id_skladu)
);

/*
 Blok tras.
 W tym bloku odbywa się tworzenie traw, odcinków i takie fajne rzeczy.
 Też są tworząne stacje
*/

CREATE TABLE stacje (
  id_stacji SERIAL PRIMARY KEY,
  ilosc_torow int NOT NULL DEFAULT 8,
  nazwa VARCHAR(255) NOT NULL,
  CHECK (ilosc_torow BETWEEN 1 AND 40)
);

-- dlugosc jest podawana w kilometrach
CREATE TABLE odcinki (
  stacja_poczatkowa int NOT NULL REFERENCES stacje (id_stacji),
  stacja_koncowa int NOT NULL REFERENCES stacje (id_stacji),
  dlugosc float NOT NULL CHECK (dlugosc > 0),

  PRIMARY KEY (stacja_poczatkowa, stacja_koncowa)
);

CREATE TABLE trasy (
  id_trasy SERIAL PRIMARY KEY,
  sklad int NOT NULL REFERENCES sklady(id_skladu),
  id_przewoznika int NOT NULL REFERENCES przewoznicy(id_przewoznika)
);

CREATE TABLE trasy_odcinki (
  id_trasy_odcinki SERIAL PRIMARY KEY,
  stacja_poczatkowa int NOT NULL,
  stacja_koncowa int NOT NULL,
  trasa int NOT NULL,
  czas_odjazdu timestamp NOT NULL,
  czas_przyjazdu timestamp NOT NULL,
  /*tor_poczatkowy int NOT NULL,
  tor_koncowy int NOT NULL,*/

  FOREIGN KEY (stacja_poczatkowa, stacja_koncowa)
    REFERENCES odcinki (stacja_poczatkowa, stacja_koncowa),
  FOREIGN KEY (trasa) REFERENCES trasy(id_trasy),

  CHECK (czas_odjazdu < trasy_odcinki.czas_przyjazdu)
);

/*
  I ostatni blok, odpowiadający za bilety, ulgi, pasażerów i podobne rzeczy
*/

CREATE TABLE ulgi (
  id_ulgi SERIAL PRIMARY KEY,
  nazwa VARCHAR(50) NOT NULL,
  procent int NOT NULL CHECK (procent BETWEEN 1 AND 100),
  obowiazuje boolean NOT NULL DEFAULT TRUE
);

CREATE TABLE pasazerowie (
  id_pasazera SERIAL PRIMARY KEY,
  imie VARCHAR(50) NOT NULL,
  nazwisko VARCHAR(50) NOT NULL,
  email VARCHAR(250)
);

-- Tu zauważmy, że pole pasażer może być NULL

CREATE TABLE bilety (
  id_biletu SERIAL PRIMARY KEY,
  id_ulgi int REFERENCES ulgi(id_ulgi),
  id_sklady_wagony INT NOT NULL REFERENCES sklady_wagony(id_sklady_wagony),
  -- NULLABLE
  miejsce_w_wagonie int,
  trasy_odcinki_od int NOT NULL REFERENCES trasy_odcinki(id_trasy_odcinki),
  trasy_odcinki_do int NOT NULL REFERENCES trasy_odcinki(id_trasy_odcinki),
  id_pasazera int REFERENCES pasazerowie(id_pasazera)
);

COMMIT;
